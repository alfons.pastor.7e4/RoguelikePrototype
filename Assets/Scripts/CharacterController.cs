﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    [SerializeField]
    Rigidbody2D rb;
    [SerializeField]
    Animator anim;
    [SerializeField]
    SpriteRenderer sr;
    float ix, iy;
    public PlayerData playerData;
    public Image HealthBar;
    
    // Start is called before the first frame update
    void Start()
    {
        playerData.currentHealth = playerData.maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        if (difference.x < 0)
        {
            sr.flipX = false;
        }
        else
        {
            sr.flipX = true;
        }
    }
    private void FixedUpdate()
    {
        ix = Input.GetAxis("Horizontal");
        iy = Input.GetAxis("Vertical");
        rb.velocity = new Vector2(ix, iy).normalized * playerData.speed;
        if (rb.velocity.magnitude > 0)
        {
            anim.SetBool("walking", true);
        }
        else
        {
            anim.SetBool("walking", false);
        }

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Door"))
        {
            LevelManager.Instance.EnteredDoor();
        }
        else if (collision.gameObject.CompareTag("EnemyBullet"))
        {
            Destroy(collision.gameObject);
            DecreaseHealth(1);
        }
    }
    
    IEnumerator Death()
    {
        GetComponent<InventoryManager>().PersistWeapons();
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(2f);
        GameManager.Instance.GameOver();
        Destroy(gameObject);
    }
    public bool IncreaseHealth(int amount)
    {
        if (playerData.currentHealth >= playerData.maxHealth)
        {
            return false;
        }
        else
        {
            playerData.currentHealth += Mathf.Clamp(amount, 0, playerData.maxHealth - playerData.currentHealth);
            HealthBar.fillAmount =  Mathf.Clamp((float) playerData.currentHealth / playerData.maxHealth, 0, 1f);
            return true;
        }
    }

    public void DecreaseHealth(int amount)
    {
        playerData.currentHealth -= amount;
        HealthBar.fillAmount =  Mathf.Clamp((float) playerData.currentHealth / playerData.maxHealth, 0, 1f);
        if (playerData.currentHealth <= 0)
        {
            StartCoroutine(Death());
        }
    }
}
