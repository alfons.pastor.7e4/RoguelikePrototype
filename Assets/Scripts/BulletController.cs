﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] private float _despawnTime = 5;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(DespawnTimer());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator DespawnTimer()
    {
        yield return new WaitForSeconds(_despawnTime);
        Destroy(gameObject);
    }
}
