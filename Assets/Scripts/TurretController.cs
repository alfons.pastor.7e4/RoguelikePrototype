﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretController : NPCController
{
    [SerializeField]
    GameObject bullet;
    GameObject instantiatedBullet;
    private bool _cooldown = false;
    [SerializeField] private float _cooldownTime = 1f;
    [SerializeField] private float _bulletSpeed = 5f;
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 difference = _player.transform.position - transform.position;
        difference.Normalize();
        float rotation_z = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotation_z - 90);

        if (!_cooldown)
        {
                _cooldown = true;
                StartCoroutine(CooldownTimer());
                instantiatedBullet = Instantiate(bullet, gameObject.transform.position, Quaternion.identity);
                Vector2 dir = _player.transform.position - instantiatedBullet.transform.position;
                instantiatedBullet.GetComponent<Rigidbody2D>().velocity = dir.normalized * _bulletSpeed;
        }
    }

    IEnumerator CooldownTimer()
    {
        yield return new WaitForSeconds(_cooldownTime);
        _cooldown = false;
    }
}
