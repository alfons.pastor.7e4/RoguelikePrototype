﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MyBuff", menuName = "ScriptableObjects/Buff", order = 1)]
public class Buff : Item
{
    public int restoredHealth;
}
