﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MyEnemy", menuName = "ScriptableObjects/Enemy", order = 1)]
public class EnemyData : CharacterData
{
    public int points;
    public int contactDamage;
    public int coinsFor;
}
