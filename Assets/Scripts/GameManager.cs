﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private GameObject _shopPrefab;
    [SerializeField] private GameObject[] _roomPrefabs;
    [SerializeField] private GameObject gameOverCanvas;
    [SerializeField] private GameObject mainMenuCanvas;
    public GameData lastGameData;
    public GameData gameData;
    public AudioClip mainTheme;
    public AudioClip deathTheme;
    
    private static GameManager _instance;
    public static GameManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<GameManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        lastGameData = PersistencyManager.Instance.LoadGameData();
        gameData = new GameData();
        gameData.topScore = lastGameData.topScore;
        gameData.coins = lastGameData.coins;
        mainMenuCanvas.GetComponent<MainMenuCanvas>().UpdateTexts();
    }

    public void addPoints(int points)
    {
        gameData.score += points;
    }

    public void AddCoins(int coins)
    {
        gameData.coins += coins;
    }
    public void LoadMainScene()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void LoadMenu()
    {
        GetComponent<AudioSource>().clip = mainTheme;
        Destroy(PersistencyManager.Instance.gameObject);
        Destroy(GameObject.FindGameObjectWithTag("LevelManager"));
        SceneManager.LoadScene("Menu");
        Destroy(gameObject);
    }

    public void GameOver()
    {
        bool isHighScore = false;
        if (gameData.score > gameData.topScore)
        {
            gameData.topScore = gameData.score;
            isHighScore = true;
        }

        GetComponent<AudioSource>().clip = deathTheme;
        GetComponent<AudioSource>().Play();
        GameObject canvas = Instantiate(gameOverCanvas);
        var controller = canvas.GetComponent<GOCanvasController>();
        controller.UpdateTexts(isHighScore);
        PersistencyManager.Instance.SaveGameData(gameData);
        lastGameData = PersistencyManager.Instance.LoadGameData();
        gameData = new GameData();
        gameData.topScore = lastGameData.topScore;
        Debug.Log("GAMEOVER");
    }
}
