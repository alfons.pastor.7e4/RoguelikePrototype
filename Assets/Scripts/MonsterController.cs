﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterController : NPCController
{
    private Vector3 _targetPosition;
    private float step;
    [SerializeField] private float speed = 2;

    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        _targetPosition = _player.transform.position;
        if ((_targetPosition - transform.position).magnitude > 1)
        {
            step = enemyData.speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, _targetPosition, step);
        }
    }
}
