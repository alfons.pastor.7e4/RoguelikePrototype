﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryManager : MonoBehaviour
{
    // Start is called before the first frame update
    public Inventory inventory;
    public Inventory startingInventory;
    public Weapon currentWeapon;
    public int currentWeaponIndex;
    public WeaponController weaponController;
    public CharacterController characterController;
    public TextMeshProUGUI coinsText;
    public GameObject[] BuffImages;
    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        //inventory = ScriptableObject.CreateInstance("Inventory") as Inventory;
        inventory = Instantiate(startingInventory);
        if (File.Exists(PersistencyManager.weaponsFile))
        {
            PersistencyManager.Instance.LoadWeapons(ref inventory);
        }
        else
        {
            PersistencyManager.Instance.SaveWeapons(inventory);
        }
        
        if (File.Exists(PersistencyManager.buffsFile))
        {
            PersistencyManager.Instance.LoadBuffs(ref inventory);
        }
        else
        {
            PersistencyManager.Instance.SaveBuffs(inventory);
        }
        currentWeaponIndex = 0;
        currentWeapon = inventory.weaponsList[currentWeaponIndex];
        UpdateCoinsText();
        UpdateBuffImages();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            currentWeaponIndex = (currentWeaponIndex < inventory.weaponsList.Count - 1) ? currentWeaponIndex += 1 : 0;
            currentWeapon = inventory.weaponsList[currentWeaponIndex];
            weaponController.UpdateWeapon();
        }
        HandleBuffs();
    }

    private void UseBuff(int index)
    {
        if (inventory.buffsList[index] != null)
        {
            if (characterController.IncreaseHealth(inventory.buffsList[index].restoredHealth))
            {
                inventory.buffsList.RemoveAt(index);
                UpdateBuffImages();
            }
        }
    }

    public void PersistWeapons()
    {
        PersistencyManager.Instance.SaveWeapons(inventory);
    }
    private void UpdateBuffImages()
    {
        for (int i = 0; i < BuffImages.Length; i++)
        {
            BuffImages[i].GetComponent<Image>().sprite = null;
        }
        for (int i = 0; i < inventory.buffsList.Count; i++)
        {
            BuffImages[i].GetComponent<Image>().sprite = inventory.buffsList[i].image;
        }
    }

    private void HandleBuffs()
    {
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            UseBuff(0);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            UseBuff(1);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            UseBuff(2);
        }
    }

    public bool BuyItem(Item item)
    {
        Debug.Log(item.GetType());
        if (GameManager.Instance.gameData.coins >= item.price)
        {
            GameManager.Instance.gameData.coins -= item.price;
            UpdateCoinsText();
            if (item.GetType() == typeof(Weapon))
            {
                inventory.weaponsList.Add((Weapon) item);
                return true;
            }
            if (item.GetType() == typeof(Buff))
            {
                if (inventory.buffsList.Count < 3)
                {
                    inventory.buffsList.Add((Buff)item);
                    UpdateBuffImages();
                    return true;
                }
            }
        }

        return false;
    }

    public void AddCoins(int coins)
    {
        GameManager.Instance.AddCoins(coins);
        UpdateCoinsText();
    }
    public void UpdateCoinsText()
    {
        coinsText.text = "Coins: " + GameManager.Instance.gameData.coins;
    }
}
