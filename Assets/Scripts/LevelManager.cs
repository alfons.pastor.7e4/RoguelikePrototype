﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private GameObject _playerPrefab;
    [SerializeField] private GameObject _shopPrefab;
    [SerializeField] private GameObject[] _roomPrefabs;
    private bool _currentIsShop = true;
    private GameObject _player;
    private bool _loading;
    public static LevelManager Instance { get; private set; }

    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            Instance = this;
        }
        DontDestroyOnLoad(gameObject);
    }
    // Start is called before the first frame update
    void Start()
    {
        Instantiate(_shopPrefab);
        _player = Instantiate(_playerPrefab, GameObject.FindGameObjectWithTag("SpawnPoint").transform.position, Quaternion.identity);

    }

    // Update is called once per frame
    void Update()
    {
        if (!_loading)
        {
            if (GameObject.FindGameObjectsWithTag("NPC").Length == 0 && GameObject.FindGameObjectWithTag("Block") != null)
            {
                GameObject.FindGameObjectWithTag("Block").SetActive(false);
            }
        }
    }

    public void EnteredDoor()
    {
        if (GameObject.FindGameObjectWithTag("Block") is null || !GameObject.FindGameObjectWithTag("Block").activeSelf)
        {
            if (!_currentIsShop)
            {
                GameManager.Instance.gameData.completedRooms += 1;
            }

            NextRoom();
        }
    }

    private void NextRoom()
    {
        _loading = true;
        StartCoroutine(LoadAsyncScene());
    }

    IEnumerator LoadAsyncScene()
    {
        DontDestroyOnLoad(_player);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync("MainScene");

        while (!asyncLoad.isDone)
        {
            yield return null;
        }
        if (_currentIsShop)
        {
            _currentIsShop = false;
            int selectedRoom = Random.Range(0, _roomPrefabs.Length);
            Instantiate(_roomPrefabs[selectedRoom]);
        }
        else
        {
            _currentIsShop = true;
            Instantiate(_shopPrefab);
        }
        //_player = Instantiate(_playerPrefab, GameObject.FindGameObjectWithTag("SpawnPoint").transform.position, Quaternion.identity);
        SceneManager.MoveGameObjectToScene(_player, SceneManager.GetActiveScene());
        _player.transform.position = GameObject.FindGameObjectWithTag("SpawnPoint").transform.position;
        Camera.main.gameObject.GetComponent<CameraController>().player = _player;
        _loading = false;
    }
}
