﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using UnityEngine;

public class PersistencyManager : MonoBehaviour
{
    public const string weaponsFile = "weapons.json";
    public const string buffsFile = "buffs.json";
    public Inventory inventory;
    public Inventory startingInventory;
    public Weapon currentWeapon;
    public int currentWeaponIndex;
    private static PersistencyManager _instance;
    public static PersistencyManager Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<PersistencyManager>();
            }

            return _instance;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        //inventory = ScriptableObject.CreateInstance("Inventory") as Inventory;
    }
    private void Update()
    {
    }
    public void SaveWeapons(Inventory inventory)
    {
        List<string> weaponNames = inventory.weaponsList.Select(n => n.name).ToList();
        string dataAsJson = JsonConvert.SerializeObject(weaponNames, Formatting.Indented);
        File.WriteAllText(weaponsFile, dataAsJson);
        
    }
    
    public void SaveBuffs(Inventory inventory)
    {
        List<string> buffNames = inventory.buffsList.Select(n => n.name).ToList();
        string buffsAsJson = JsonConvert.SerializeObject(buffNames, Formatting.Indented);
        File.WriteAllText(buffsFile, buffsAsJson);
    }
    public void LoadWeapons(ref Inventory inventory)
    {
        inventory.weaponsList.Clear();
        string weaponsStr = File.ReadAllText(weaponsFile);
        var weaponNames = JsonConvert.DeserializeObject<List<string>>(weaponsStr);
        foreach (var n in weaponNames)
        {
            List<Weapon> weapons = Resources.LoadAll<Weapon>("ScriptableObjects/Weapons/").ToList();
            inventory.weaponsList.Add(weapons.Where(w => w.name == n).First());
        }
    }
    
    public void LoadBuffs(ref Inventory inventory)
    {
        inventory.buffsList.Clear();
        string buffsStr = File.ReadAllText(buffsFile);
        var buffNames = JsonConvert.DeserializeObject<List<string>>(buffsStr);
        foreach (var n in buffNames)
        {
            List<Buff> buffs = Resources.LoadAll<Buff>("ScriptableObjects/buffs/").ToList();
            inventory.buffsList.Add(buffs.Where(w => w.name == n).First());
        }
    }

    public void SaveGameData(GameData data)
    {
        string jsonData = JsonConvert.SerializeObject(data, Formatting.Indented);
        File.WriteAllText("data.json", jsonData);
    }
    
    public GameData LoadGameData()
    {
        string jsonData = File.ReadAllText("data.json");
        return JsonConvert.DeserializeObject<GameData>(jsonData);
    }
    
}
