﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class GameData
{
    public int topScore;
    public int score;
    public int defeatedEnemies;
    public int completedRooms;
    public int coins;
}
