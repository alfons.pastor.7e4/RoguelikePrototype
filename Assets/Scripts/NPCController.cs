﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class NPCController : MonoBehaviour
{
    public GameObject _player;

    public EnemyData enemyData;

    private bool dead = false;
    // Start is called before the first frame update
    protected void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");
        enemyData.currentHealth = enemyData.maxHealth;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("PlayerBullet"))
        {
            enemyData.currentHealth -= 1;
            Destroy(collision.gameObject);
            if (enemyData.currentHealth <= 0)
            {
                GameManager.Instance.addPoints(enemyData.points);
                _player.GetComponent<InventoryManager>().AddCoins(enemyData.coinsFor);
                GameManager.Instance.gameData.defeatedEnemies += 1;
                Destroy(gameObject);
            }
        }

        if (collision.CompareTag("Player") && enemyData.contactDamage > 0)
        {
            if (!dead)
            {
                dead = true;
                collision.GetComponent<CharacterController>().DecreaseHealth(enemyData.contactDamage);
                StartCoroutine(Death());
            }
        }
    }
    
    IEnumerator Death()
    {
        GetComponent<AudioSource>().Play();
        yield return new WaitForSeconds(0.5f);
        Destroy(gameObject);
    }
}
