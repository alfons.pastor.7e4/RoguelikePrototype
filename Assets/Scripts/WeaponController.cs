﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    [SerializeField]
    GameObject bullet;
    GameObject instantiatedBullet;
    [SerializeField]
    SpriteRenderer sr;
    CharacterController cc;
    private bool _cooldown;
    [SerializeField] float _cooldownTime;
    [SerializeField] float _bulletSpeed;
    [SerializeField] Sprite _weaponSprite;

    public InventoryManager inventoryManager;
    // Start is called before the first frame update
    void Start()
    {
        UpdateWeapon();
    }

    // Update is called once per frame
    void Update()
    {
        if (!_cooldown)
        {
            if (Input.GetMouseButton(0))
            {
                _cooldown = true;
                StartCoroutine(CooldownTimer());
                GetComponent<AudioSource>().Play();
                instantiatedBullet = Instantiate(bullet, gameObject.transform.position, Quaternion.identity);
                Vector2 dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - instantiatedBullet.transform.position;
                instantiatedBullet.GetComponent<Rigidbody2D>().velocity = dir.normalized * _bulletSpeed;
            }
        }
        Vector3 difference = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        difference.Normalize();
        float rotation_z = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, rotation_z + 0);
        if (transform.rotation.eulerAngles.z > 90 && transform.rotation.eulerAngles.z < 270)
        {
            sr.flipY = true;
        }
        else
        {
            sr.flipY = false;
        }
    }

    private void FixedUpdate()
    {
    }

    public void UpdateWeapon()
    {
        _cooldownTime = 1f / inventoryManager.currentWeapon.fireRate;
        _bulletSpeed = inventoryManager.currentWeapon.bulletSpeed;
        sr.sprite = inventoryManager.currentWeapon.image;
    }
    IEnumerator CooldownTimer()
    {
        yield return new WaitForSeconds(_cooldownTime);
        _cooldown = false;
    }
}
