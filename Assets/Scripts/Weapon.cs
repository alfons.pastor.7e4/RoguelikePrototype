﻿using System;
using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "MyWeapon", menuName = "ScriptableObjects/Weapon", order = 1)]
public class Weapon : Item
{
    public float bulletSpeed;
    public float fireRate;
}