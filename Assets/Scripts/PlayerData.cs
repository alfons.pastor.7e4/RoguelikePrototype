﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "MyPlayer", menuName = "ScriptableObjects/Player", order = 1)]
public class PlayerData : CharacterData
{
    public Inventory inventory;
    public int currentCoins;
    public int accumulatedCoins;
    public int currentPoints;
    public int accumulatedPoints;
}
