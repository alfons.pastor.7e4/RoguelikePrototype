﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ItemTypes
{
    Weapon,
    Buff
}
public abstract class Item : ScriptableObject
{
    public string Name;
    public string description;
    public int price;
    public float chance;
    public ItemTypes itemType;
    public Sprite image;

}
