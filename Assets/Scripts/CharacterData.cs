﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class CharacterData : ScriptableObject
{
    public string Name;
    public int currentHealth;
    public int maxHealth;
    public float speed;

}
