﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MainMenuCanvas : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI topScoreText;
    [SerializeField] private TextMeshProUGUI lastScoreText;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateTexts()
    {
        topScoreText.text = $"Best Score: {GameManager.Instance.gameData.topScore}";
        lastScoreText.text = $"Last Score: {GameManager.Instance.lastGameData.score}";
    }
}
