﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopManager : MonoBehaviour
{
    public Transform[] itemSpawns;
    public Weapon[] weapons;
    public Buff[] buffs;
    public GameObject shopItem;
    // Start is called before the first frame update
    void Start()
    {
        GameObject weaponSpawn = Instantiate(shopItem, itemSpawns[0]);
        weaponSpawn.GetComponent<ShopItem>().item = weapons[Random.Range(0, weapons.Length)];
        weaponSpawn.GetComponent<ShopItem>().UpdateItem();
        for(int i = 1; i < itemSpawns.Length; i++)
        {
            GameObject itemSpawn = Instantiate(shopItem, itemSpawns[i]);
            itemSpawn.GetComponent<ShopItem>().item = buffs[Random.Range(0, buffs.Length)];
            itemSpawn.GetComponent<ShopItem>().UpdateItem();
        }

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
