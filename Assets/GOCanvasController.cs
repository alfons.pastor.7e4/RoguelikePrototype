﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GOCanvasController : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI topScoreText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI enemiesText;
    [SerializeField] private TextMeshProUGUI completedRoomsText;
    [SerializeField] private TextMeshProUGUI highScoreText;
    [SerializeField] private Button exitButton;

    // Start is called before the first frame update
    void Start()
    {
        
        exitButton.onClick.AddListener(() => GameManager.Instance.LoadMenu());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UpdateTexts(bool isHighScore)
    {
        topScoreText.text = $"Top Score: {GameManager.Instance.gameData.topScore}";
        scoreText.text = $"Score: {GameManager.Instance.gameData.score}";
        enemiesText.text = $"Defeated Enemies: {GameManager.Instance.gameData.defeatedEnemies}";
        completedRoomsText.text = $"Completed Rooms: {GameManager.Instance.gameData.completedRooms}";
        if (isHighScore)
        {
            highScoreText.text = $"You have beaten the top Score!";
        }
    }
}
