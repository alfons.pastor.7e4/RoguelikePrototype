﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour
{
    // Start is called before the first frame update
    public Item item;
    [SerializeField] private TextMeshProUGUI nameText;
    [SerializeField] private TextMeshProUGUI priceText;
    [SerializeField] private Image image;
    void Start()
    {

    }

    public void UpdateItem()
    {
        nameText.text = $" {item.name}";
        priceText.text = $"Price: {item.price}";
        image.sprite = item.image;
    }
    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Player"))
        {
            if (col.GetComponent<InventoryManager>().BuyItem(item))
            {
                Destroy(gameObject);
            }
        }
    }
}
